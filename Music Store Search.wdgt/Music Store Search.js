// Music Store Search
// Copyright (c) 2007 Alien Orb Software LLC. All rights reserved.
// Created using Dashcode.

//
// Function: load()
// Called by HTML body element's onload event when the widget is ready to start
//
function load()
{
    setupParts();
}

//
// Function: showBack(event)
// Called when the info button is clicked to show the back of the widget
//
// event: onClick event from the info button
//
function showBack(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

//
// Function: showFront(event)
// Called when the done button is clicked from the back of the widget
//
// event: onClick event from the done button
//
function showFront(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function search( searchEvent )
{
	if (searchEvent.keyCode != 13)
		return;

	var searchTerm = searchEvent.target.value;
	
	if (searchTerm.length > 0)
	{
		var amazonURL = "http://www.amazon.com/s/?initialSearch=1&url=search-alias%3Ddigital-music&field-keywords=" + encodeURIComponent( searchTerm ) + "&x=0&y=0";
		var iTunesURL = "http://phobos.apple.com/WebObjects/MZSearch.woa/wa/search?submit=seeAllLockups&term=" + encodeURIComponent( searchTerm ) + "&media=music";
	
		if (window.widget)
		{
			widget.openURL( iTunesURL );
			widget.openURL( amazonURL );
		}
	}
}

if (window.widget) {
//    widget.onremove = remove;
//    widget.onhide = hide;
//    widget.onshow = show;
//    widget.onsync = sync;
}


function openAlienOrbSite(event)
{
    if (window.widget)
		widget.openURL( "http://alienorb.com/" );
}
