/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

//
// Music Store Search
// 2007-2009 by Alien Orb Software LLC
// Created using Dashcode.
//

function load()
{
    setupParts();
}

function showBack( event )
{
    var front = document.getElementById( "front" );
    var back = document.getElementById( "back" );

    if (window.widget)
        widget.prepareForTransition( "ToBack" );

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget)
        setTimeout( 'widget.performTransition();', 0 );
}

function showFront( event )
{
    var front = document.getElementById( "front" );
    var back = document.getElementById( "back" );

    if (window.widget)
        widget.prepareForTransition( "ToFront" );

    front.style.display = "block";
    back.style.display = "none";

    if (window.widget)
        setTimeout( 'widget.performTransition();', 0 );
}

function search( searchEvent )
{
	if (searchEvent.keyCode != 13)
		return;

	var searchTerm = searchEvent.target.value;
	
	if (searchTerm.length > 0)
	{
		var amazonURL = "http://www.amazon.com/s/?initialSearch=1&url=search-alias%3Ddigital-music&field-keywords=" + encodeURIComponent( searchTerm ) + "&x=0&y=0";
		var iTunesURL = "http://phobos.apple.com/WebObjects/MZSearch.woa/wa/search?submit=seeAllLockups&term=" + encodeURIComponent( searchTerm ) + "&media=music";
        var walmartURL = "http://mp3.walmart.com/store/search?action=all&keyword=" + encodeURIComponent( searchTerm );
	
		if (window.widget)
		{
			widget.openURL( iTunesURL );
			widget.openURL( amazonURL );
            widget.openURL( walmartURL );
		}
	}
}

function openAlienOrbSite( event )
{
    if (window.widget)
		widget.openURL( "http://alienorb.com/" );
}
